<?php

class DataLoader
{
	private array $fileList = [];
	private array $concatenatedRawData = [];
	private array $formattedDataCollection = [];
	
	public function init(): void
	{
		$this->loadFiles();
		$this->concatData();
		$this->formatData();
	}

	private function loadFiles(): void
	{
		$sourceDir = getenv("UNIT_GRAPH_DATA_SOURCE");
		if (!is_string($sourceDir)) {
			$message = '[ERROR] env `UNIT_GRAPH_DATA_SOURCE` was not found - cant load any data source';
			error_log($message);
			print($message . PHP_EOL);
			return;
		}
		$this->fileList = explode("\n", shell_exec("find '${sourceDir}' -type f -name \"*.json\""));
		$this->fileList = array_filter($this->fileList);
	}

	private function concatData(): void
	{
		$this->concatenatedRawData = [];
		foreach ($this->fileList as $filePath) {
			try {
				$data = json_decode(file_get_contents($filePath), true, 512, JSON_THROW_ON_ERROR);
				foreach ($data as $entry) {
					$this->concatenatedRawData[] = $entry;
				}
			} catch (Exception $ex) {
				echo '<pre>' . print_r($ex, true) . '</pre>';
			}
		}
	}

	private function formatData(): void
	{
		$this->formattedDataCollection = [];
		$this->loadDataPoints();
		$this->sortDataPoints();
	}

	private function isValidDatapoint(array &$dataPoint): bool
	{
		return array_key_exists('unit', $dataPoint) && $this->isUnitString($dataPoint['unit']) &&
		       array_key_exists('date', $dataPoint) && $this->isDateString($dataPoint['date']) &&
		       array_key_exists('value', $dataPoint) && $this->isNumericValue($dataPoint['value']);
	}

	private function isDateString(string &$dateString): bool
	{
		return true;
	}

	private function isUnitString(string &$unit): bool
	{
		return true;
	}

	private function isNumericValue(string &$value): bool
	{
		return true;
	}

	private function loadDataPoints(): void
	{
		foreach ($this->concatenatedRawData as $inputDataPoint) {
			if ($this->isValidDatapoint($inputDataPoint) === true) {
				$this->addDataPoint($inputDataPoint);
			}
		}
	}

	private function sortDataPoints(): void
	{
		foreach ($this->getUnitList() as $unit) {
			usort($this->formattedDataCollection[$unit], function ($a, $b) {
				return $a['date'] <=> $b['date'];
			});
		}
	}

	private function addDataPoint(array &$dataPoint): void
	{
		if (array_key_exists($dataPoint['unit'], $this->formattedDataCollection) === false) $this->formattedDataCollection[$dataPoint['unit']] = [];

		$timeStamp = new DateTime($dataPoint['date']);
		$timeStamp = $timeStamp->getTimestamp();
		$this->formattedDataCollection[$dataPoint['unit']][] = ['date' => $timeStamp, 'value' => (float) $dataPoint['value']];
	}

	public function getUnitList(): array
	{
		return array_keys($this->formattedDataCollection);
	}

	public function printFormattedData(): void
	{
		echo "<pre>" . print_r($this->formattedDataCollection, true) . "</pre>";
	}

	public function getFormattedDataAsJson(): string
	{
		return json_encode($this->formattedDataCollection);
	}
}
