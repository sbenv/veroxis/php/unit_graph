Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function round_one_decimal(value)
{
	return Math.round(value * 10) / 10;
}

function round_two_decimal(value)
{
	return Math.round(value * 100) / 100;
}

function date_to_string(date)
{
	let year = date.getFullYear();
	let month = (date.getMonth() + 1).toString().padStart(2, '0');
	let day = date.getDate().toString().padStart(2, '0');
	return year + '-' + month + '-' + day;
}

function date_to_month_string(date)
{
	return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0');
}

function getStatisticsByUnit(unit)
{
	let returnData = {'unit': unit};

	let minDate = new Date(document.getElementById('start_date').value).getTime() / 1000;
	let maxDate = new Date(document.getElementById('end_date').value).getTime() / 1000;
	maxDate += 86400;

	let counter = 0;
	let firstDate = Number.MAX_VALUE;
	let lastDate = 0;
	let lowestValue = Number.MAX_VALUE;
	let lowestValueDate = 0;
	let highestValue = 0;
	let highestValueDate = 0;
	let newestDate = 0;
	let newestValue = 0;

	document.rawDataFromBackend[unit].forEach((data) => {
		if (data['date'] >= minDate && data['date'] <= maxDate) {
			counter++;
			firstDate = data['date'] < firstDate ? data['date'] : firstDate;
			lastDate = data['date'] > lastDate ? data['date'] : lastDate;
			if (data['value'] < lowestValue) {
				lowestValue = data['value'];
				lowestValueDate = data['date'];
			}
			if (data['value'] > highestValue) {
				highestValue = data['value'];
				highestValueDate = data['date'];
			}
			if (newestDate < data['date']) {
				newestDate = data['date'];
				newestValue = data['value'];
			}
		}
	});

	lowestValueDate = date_to_string(new Date(lowestValueDate * 1000));
	highestValueDate = date_to_string(new Date(highestValueDate * 1000));

	let title = `Dataset for: [${unit}]`;

	returnData[title] = {};
	returnData[title]['Timespan (selected)'] = date_to_string(new Date(minDate * 1000)) + ' --> ' + date_to_string(new Date((maxDate - 86400) * 1000));
	returnData[title]['Timespan (data available)'] = date_to_string(new Date(firstDate * 1000)) + ' --> ' + date_to_string(new Date(lastDate * 1000));
	returnData[title]['Amount Datepoints'] = counter;
	returnData[title][`Highest Value (${highestValueDate})`] = highestValue;
	returnData[title][`Lowest Value (${lowestValueDate})`] = lowestValue;
	returnData[title][`Newest Value (${date_to_string(new Date(newestDate * 1000))})`] = newestValue;

	returnData['Datapoints per Month'] = {};
	document.rawDataFromBackend[unit].forEach((data) => {
		if (data['date'] >= minDate && data['date'] <= maxDate) {
			let dateString = date_to_month_string(new Date(data['date'] * 1000));
			if (!(dateString in returnData['Datapoints per Month'])) returnData['Datapoints per Month'][dateString] = 0;
			returnData['Datapoints per Month'][dateString]++;
		}
	});

	returnData['Average Value per Day'] = {};
	let counterObject = {};
	let sumObject = {};
	document.rawDataFromBackend[unit].forEach((data) => {
		if (data['date'] >= minDate && data['date'] <= maxDate) {
			let dateString = date_to_string(new Date(data['date'] * 1000));
			if (!(dateString in counterObject)) counterObject[dateString] = 0;
			if (!(dateString in sumObject)) sumObject[dateString] = 0;
			counterObject[dateString]++;
			sumObject[dateString] += data['value'];
		}
	});
	for (let [key, value] of Object.entries(sumObject)) {
		returnData['Average Value per Day'][key] = round_two_decimal(value / counterObject[key]);
	};

	returnData['Average Value per Month'] = {};
	counterObject = {};
	sumObject = {};
	for (let [key, value] of Object.entries(returnData['Average Value per Day'])) {
			let dateString = key.substring(0, key.length - 3);
			if (!(dateString in counterObject)) counterObject[dateString] = 0;
			if (!(dateString in sumObject)) sumObject[dateString] = 0;
			counterObject[dateString]++;
			sumObject[dateString] += value;
	};
	for (let [key, value] of Object.entries(sumObject)) {
		returnData['Average Value per Month'][key] = round_two_decimal(value / counterObject[key]);
	};

	returnData['Average Value per Year'] = {};
	counterObject = {};
	sumObject = {};
	for (let [key, value] of Object.entries(returnData['Average Value per Day'])) {
			let dateString = key.substring(0, key.length - 6);
			if (!(dateString in counterObject)) counterObject[dateString] = 0;
			if (!(dateString in sumObject)) sumObject[dateString] = 0;
			counterObject[dateString]++;
			sumObject[dateString] += value;
	};
	for (let [key, value] of Object.entries(sumObject)) {
		returnData['Average Value per Year'][key] = round_two_decimal(value / counterObject[key]);
	};

	return returnData;
}

function createDatePickers()
{
	let today = new Date();
	let defaultStartDate = new Date();
	defaultStartDate.setFullYear(defaultStartDate.getFullYear() - 10);

	let container = document.getElementById('constant_page_header');

	let label = document.createElement('span');
	label.innerHTML = 'Timespan:'

	let start = document.createElement('input');
	start.type = 'date';
	start.id = 'start_date';
	start.required = true;
	start.value = date_to_string(defaultStartDate);
	start.onchange = () => {
		destroyDom();
		loadDom();
	};

	let end = document.createElement('input');
	end.type = 'date';
	end.id = 'end_date';
	end.required = true;
	end.value = date_to_string(today);
	end.onchange = () => {
		destroyDom();
		loadDom();
	};

	container.appendChild(label);
	container.appendChild(start);
	container.appendChild(end); 
}

function createRootElement()
{
	let rootElement = document.createElement('section');
	rootElement.id = 'root';
	document.body.appendChild(rootElement);
}

function getLinechartData(unit, minDate, maxDate)
{
	let buffer = [];
	document.rawDataFromBackend[unit].forEach((data) => {
		if (data['date'] >= minDate && data['date'] <= maxDate) {
			buffer.push({
				x: new Date(data['date'] * 1000),
				y: data['value']
			});
		}
	});
	return buffer;
}

function createStatisticsElement(statistics)
{
	let container = document.createElement('div');
	container.classList.add('stat_block');

	let list = document.createElement('ul');

	// Categorized Data Output
	for (let [key, value] of Object.entries(statistics)) {
		if (typeof value === 'object' && !(value instanceof Date)) {
			let category_container = document.createElement('li');
			category_container.classList.add('category_container');
			let title = document.createElement('div');
			title.classList.add('category_title');
			title.innerHTML = `${key}`;
			category_container.appendChild(title);
			let inner_container = document.createElement('div');
			inner_container.classList.add('category_content_container');
			// console.log(`-- ${key} --`);
			for (let [sub_key, sub_value] of Object.entries(value)) {
				let printVal = sub_value;
				if (printVal instanceof Date) printVal = date_to_string(printVal);
				// console.log(`  ${sub_key} => ${sub_value}`);
				let row = document.createElement('div');
				row.classList.add('category_row');

				let key = document.createElement('span');
				key.classList.add('category_key');
				key.innerHTML = `${sub_key}`;

				let value = document.createElement('span');
				value.classList.add('category_value');
				value.innerHTML = `${printVal}`;

				row.appendChild(key);
				row.appendChild(value);
				inner_container.appendChild(row);
			}
			category_container.appendChild(inner_container);
			list.appendChild(category_container);
		}
	}

	let chartContainer = document.createElement('div');
	chartContainer.classList.add("chart");

	let lineChart = document.createElement('canvas');

	chartContainer.appendChild(lineChart);
	container.appendChild(list);
	container.appendChild(chartContainer);
	document.getElementById('root').appendChild(container);

	let minDate = new Date(document.getElementById('start_date').value).getTime() / 1000;
	let maxDate = new Date(document.getElementById('end_date').value).getTime() / 1000;
	maxDate += 86400;

	let unit = statistics['unit'];
	new Chart(lineChart.getContext('2d'), {
		type: 'line',
		data: {
			labels: [unit],
			datasets: [
				{
					label: unit,
					backgroundColor: 'hsla(200, 80%, 45%, 0.05)',
					borderColor: 'hsla(200, 80%, 45%, 1)',
					data: getLinechartData(unit, minDate, maxDate),
					fill: true,
				}
			]
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
			legend: {
				display: false
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					type: 'time',
					scaleLabel: {
						display: false,
						labelString: 'Date'
					},
					ticks: {
						beginAtZero: false,
						source: "data",
					},
					time: {
						parser: 'YYYY-MM-DD'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: false,
						labelString: 'data'
					},
					ticks: {
						beginAtZero: false
					}
				}]
			}
		}
	});
}

function findGetParameter(parameterName)
{
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return String(result);
}

function generateDataSelector(targetId, optionList)
{
	let outer_container = document.createElement('div');
	outer_container.classList.add('searchbox');

	let input = document.createElement('input');
	input.id = 'data_selector';
	const queriedDataSet = findGetParameter('dataset');
	if (queriedDataSet === 'null') {
		input.value = optionList[0];
	} else {
		input.value = queriedDataSet;
	}
	input.readOnly = true;
	input.classList.add('value_box');

	let search_container = document.createElement('div');
	search_container.classList.add('dropdown');

	let filter_input = document.createElement('input');
	let searchbox_is_visible = false;
	let search_options = document.createElement('div');
	search_options.classList.add('option_list');
	let search_option_element_list = [];
	optionList.forEach((value) => {
		let search_option = document.createElement('div');
		search_option.classList.add('search_option');
		search_option.innerHTML = value;
		search_option.style.display = 'block';
		search_option.onclick = (event) => {
			input.value = event.target.innerHTML;
			filter_input.value = '';
			search_container.style.display = 'none';
			searchbox_is_visible = false;
			search_option_element_list.forEach((element) => {
				element.style.display = 'block';
			});
			destroyDom();
			loadDom();
		}
		search_option_element_list.push(search_option);
		search_options.appendChild(search_option);
	});

	filter_input.classList.add('filter_input');
	filter_input.onkeyup = (event) => {
		if (event.keyCode === 13) {
			let found = false;
			search_option_element_list.forEach((element) => {
				if (found === false && element.style.display === 'block') {
					input.value = element.innerHTML;
					found = true;
				}
			});
			filter_input.value = '';
			search_container.style.display = 'none';
			searchbox_is_visible = false;
			search_option_element_list.forEach((element) => {
				element.style.display = 'block';
			});
			destroyDom();
			loadDom();
			return;
		}
		let user_input_string = event.target.value;
		search_option_element_list.forEach((element) => {
			if (user_input_string === '') {
				element.style.display = 'block';
			} else {
				if (element.innerHTML.includes(user_input_string)) {
					element.style.display = 'block';
				} else {
					element.style.display = 'none';
				}
			}
		});
	};

	input.onchange = () => {
		destroyDom();
		loadDom();
	};

	input.onclick = () => {
		if (searchbox_is_visible === false) {
			search_container.style.display = 'grid';
			filter_input.focus();
			searchbox_is_visible = true;
		} else {
			search_container.style.display = 'none';
			searchbox_is_visible = false;
		}
	};

	outer_container.appendChild(input);
	search_container.appendChild(filter_input);
	search_container.appendChild(search_options);
	outer_container.appendChild(search_container);
	return outer_container;
}

function createDataSelector() {
	let container = document.getElementById('constant_page_header');

	let amountDatasets = Object.size(document.rawDataFromBackend);
	let label = document.createElement('span');
	label.innerHTML = `Dataset (${amountDatasets} available)`;

	const orderedData = Object.keys(document.rawDataFromBackend).sort().reduce(
		(obj, key) => {
			obj[key] = document.rawDataFromBackend[key];
			return obj;
		},
		{}
	);

	container.appendChild(label);
	container.appendChild(generateDataSelector('data_selector', Object.keys(orderedData)));
	document.body.appendChild(container);
}

function createConstantPageHeader() {

	let container = document.createElement('section');
	container.id = 'constant_page_header';
	document.body.appendChild(container);
}

function init()
{
	createConstantPageHeader();
	createDataSelector();
	createDatePickers();
	createRootElement();
}

function loadDom()
{
	let data_selector = document.getElementById('data_selector');
	let data_selected = data_selector.value;
	if (data_selected in document.rawDataFromBackend) {
		createStatisticsElement(getStatisticsByUnit(data_selected));
	} else {
		document.getElementById('root').innerHTML = 'No Data available for [' + data_selected + ']';
	}
}

function destroyDom()
{
	let rootElement = document.getElementById('root');
	rootElement.innerHTML = '';
}

init();
loadDom();

document.body.onresize = () => {
	destroyDom();
	loadDom();
};
