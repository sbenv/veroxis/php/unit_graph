<?php
	require_once(__DIR__ . '/../src/DataLoader.php');
	date_default_timezone_set('Europe/Berlin'); 
	$dataLoader = new DataLoader();
	$dataLoader->init();
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Generic Stat Tracker</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="css/normalize.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<script src="js/moment.min.js"></script>
	<script src="js/Chart.bundle.min.js"></script>
	<script>
		document.rawDataFromBackend = JSON.parse('<?= $dataLoader->getFormattedDataAsJson() ?>');
	</script>
</head>

<body>
	<script src="js/app.js"></script>
</body>

</html>
