#!/bin/sh

set -eux

docker build --pull -t "registry.gitlab.com/sbenv/veroxis/php/unit_graph:latest" .
